#include "./MaxMatrix/MaxMatrix.cpp" // Biblioteket er hentet fra github https://github.com/riyas-org/max7219

// Spillvariabler
bool gameOver = false;
int buttonInput = 0;
int rightButton = 2;
int leftButton = 3;
int coordMax = 7;
int coordMin = 0;
int tick = 750;
int xp = 0;

// Slangevariabler
int snake[64][2];
int snakeLength = 4;
int bodyIndex = -1;
int headDirection = 0;
int headPosition[2] = {0, 3};
int directions[4][2] = {
  { 1, 0},
  { 0,-1},
  {-1, 0},
  { 0, 1}
};

// MaxMatrix variabler og objektinstansiering
const int DIN = 11;
const int CS = 10;
const int CLK = 13;
MaxMatrix matrix(DIN, CS, CLK, 1);

// Funksjoner
void moveInDirection(int y[2]) {
    headPosition[0] = headPosition[0] + y[0];
    headPosition[1] = headPosition[1] + y[1];
}

int updateDirection(int currentDirection) {
  if (buttonInput != 0) {
    if(currentDirection == 3 && buttonInput == 1) return 0;
    if (currentDirection == 0 && buttonInput == -1) return 3;
    else {
      return currentDirection + buttonInput;
    }
  }
}

int clamp(int value, int minimum, int maximum) {
  if (value > maximum) return maximum;
  if (minimum > value) return minimum;
  else return value;
}

// Program
void setup() {
  matrix.init();
  matrix.setIntensity(5);
  snake[bodyIndex][0] = headPosition[0];
  snake[bodyIndex][1] = headPosition[1];
  pinMode(leftButton, INPUT);
  pinMode(rightButton, INPUT);
  Serial.begin(9600);
  delay(2000);
}

void loop() {
  if (!gameOver) {
    matrix.clear();
    buttonInput = 0;
    if (digitalRead(leftButton) == HIGH) buttonInput = -1;
    if (digitalRead(rightButton) == HIGH) buttonInput = 1;

    headDirection = updateDirection(headDirection);
    headDirection = clamp(headDirection, 0, 3);
    moveInDirection(directions[headDirection]);

    bodyIndex += 1;
    snake[bodyIndex][0] = headPosition[0];
    snake[bodyIndex][1] = headPosition[1];
    if (bodyIndex >= snakeLength) {
      bodyIndex = 0;  
    }

    xp += 1;
    if (xp >= 10) {
      tick -= 10;
      snakeLength += 1;
      xp = 0;
    }

    Serial.println(String(headDirection) + " " + " position: " + String(headPosition[0]) + "," + String(headPosition[1]));

    for (int i = 0; i <= 64; i++) {
      if (i > snakeLength) break;
      matrix.setDot(snake[i][0], snake[i][1], true);
    }
    
    if (headPosition[0] > coordMax or headPosition[0] < coordMin) {
      gameOver = true;
    } else if (headPosition[1] > coordMax or headPosition[1] < coordMin) {
	  gameOver = true;
	}
    
    delay(tick);
  }
  else {
    matrix.clear();
  }
}
